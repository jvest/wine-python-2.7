# Python27 / Pyinstaller in Wine

## Description

Environment in Wine to use python with pyinstaller.  Specifically allows building Windows binaries using Pyinstaller in Linux

## Installation/Setup

```
setup.sh
```

